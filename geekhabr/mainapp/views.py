from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .models import *


def index(request):
    articles = Article.objects.all()
    categories = Category.objects.all()
    context = {
        'title': 'Главная',
        'articles': articles,
        'categories': categories
    }
    return render(request, 'mainapp/index.html', context=context)


def design(request):
    return render(request, 'mainapp/design.html', {'title': 'Дизайн'})


def web_dev(request):
    return render(request, 'mainapp/web_dev.html',
                  {'title': 'Веб разработка'})


def mobile_dev(request):
    return render(request, 'mainapp/mobile_dev.html',
                  {'title': 'Мобильная разработка'})


def marketing(request):
    return render(request, 'mainapp/marketing.html', {'title': 'Маркетинг'})


def show_article(request, article_id):
    return HttpResponse(f'Article id: {article_id}')


class RegisterUser(CreateView):
    form_class = UserCreationForm
    template_name = 'mainapp/register.html'
    success_url = reverse_lazy('home')
