from django.db import models
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=20, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['id']
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Article(models.Model):
    title = models.CharField(max_length=255)
    user = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    content = models.TextField(blank=False)
    img = models.ImageField(upload_to='images/%Y/%m/%d/')
    time_of_creation = models.DateTimeField(auto_now_add=True)
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    is_published = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('article', kwargs={'article_id': self.pk})

    class Meta:
        verbose_name = 'Хабр'
        verbose_name_plural = 'Хабры'
        ordering = ['time_of_creation', 'title']
