from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('design/', design, name='design'),
    path('web_dev/', web_dev, name='web_dev'),
    path('mobile_dev/', mobile_dev, name='mobile_dev'),
    path('marketing/', marketing, name='marketing'),
    path('article/<int:article_id>/', show_article, name='article'),
    path('register/', RegisterUser.as_view(), name='register')
]
